﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEditor;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class MaterialDesignCanvas : MonoBehaviour
{
	[TitleGroup("Layout", "Everything you'll need to place & adapt your materials")]
	[Tooltip("The path where you store your breakpoints")]
	[Required, OnValueChanged("UpdateBreakpoints")]
	[FolderPath(RequireExistingPath = true)]
	public string BreakpointsPath;
	public Color marginsColor;
	public Color gutterColor;
	public Color gridColor;
	[ToggleLeft]
	public bool showGrid = true;
	[ToggleLeft, EnableIf("showGrid")]
	public bool drawSmallGrid;
	
	
	[TitleGroup("Debug", "You can ignore that as long as you don't have any error")]
	[ReadOnly, ShowInInspector]
	BreakpointsScriptableObject actualBreakPoint;
	
	[ReadOnly, ShowInInspector]
	List<BreakpointsScriptableObject> myBreakpoints;
	
	[ReadOnly, ShowInInspector]
	[TableList(DrawScrollView = true, MaxScrollViewHeight = 200, MinScrollViewHeight = 100)]
	List<DesignMaterial> myMats;
	
	[ReadOnly, ShowInInspector]
	Vector3[] corners;
	
	[ReadOnly, ShowInInspector]
	float unitToDp;
	
	[ReadOnly, ShowInInspector]
	int trueDp;
	
	[ReadOnly, ShowInInspector]
	int usedDp;
	
	[ReadOnly, ShowInInspector]
	int density;
	
	[ReadOnly, ShowInInspector]
	Vector2 screenResolution;
	
	[ReadOnly, ShowInInspector]
	[SuffixLabel("dp", Overlay = true)]
	int gridSize = 8;
	
	[ReadOnly, ShowInInspector]
	[SuffixLabel("px", Overlay = true)]
	float unityGridSize;
	
	[ReadOnly, ShowInInspector]
	bool breakPointsOrdered = false;
	
	void ConvertDpToUnits()
	{
		unitToDp = screenResolution.x/usedDp;
		unityGridSize = gridSize * unitToDp;
	}



	void OnDrawGizmosSelected()
	{
		UpdateResolution();
		DrawMargins();
		if(showGrid)
		{
			DrawGrid();			
		}
		ConvertDpToUnits();
	}
	
	void UpdateResolution()
	{
		if(drawSmallGrid)
		{
			gridSize = 4;
		}
		
		else
		{
			gridSize = 8;
		}
		
		#if UNITY_EDITOR
		//screenResolution.x = DisplayMetricsMaterial.WidthPixels;
		//screenResolution.y = DisplayMetricsMaterial.HeightPixels;
		screenResolution.x = GetComponent<RectTransform>().sizeDelta.x;
		screenResolution.y = GetComponent<RectTransform>().sizeDelta.y;
		trueDp = ((int)screenResolution.x * 160) / DisplayMetricsMaterial.DensityDPI;
		#endif
		
		#if UNITY_ANDROID
		screenResolution.x = GetComponent<RectTransform>().sizeDelta.x;
		screenResolution.y = GetComponent<RectTransform>().sizeDelta.y;
		trueDp = ((int)screenResolution.x * 160) / DisplayMetricsMaterial.DensityDPI;
		#endif
		
		corners = new Vector3[4];
		GetComponent<RectTransform>().GetWorldCorners(corners);
		
		SelectActualBreakpoint();
	}
	
	void DrawMargins()
	{
		Rect leftGutter = new Rect(0,0, actualBreakPoint.margin * unitToDp, corners[2].y);
		Rect rightGutter = new Rect(corners[3].x - actualBreakPoint.margin * unitToDp,0, actualBreakPoint.margin * unitToDp, corners[2].y);
		Gizmos.color = marginsColor;
		Gizmos.DrawCube(new Vector3(leftGutter.center.x, leftGutter.center.y, 0.01f), new Vector3(leftGutter.size.x, leftGutter.size.y, 0.01f));
		Gizmos.DrawCube(new Vector3(rightGutter.center.x, rightGutter.center.y, 0.01f), new Vector3(rightGutter.size.x, rightGutter.size.y, 0.01f));
	}
	
	void DrawColumns()
	{
		
	}
	
	void DrawGrid()
	{
		Gizmos.color = gridColor;
		Vector3 start = Vector3.zero;
		Vector3 end = new Vector3(0, screenResolution.y, 0);
		for (float i = 0; i <= Mathf.RoundToInt(screenResolution.x); i+= unityGridSize)
		{
			start.x= i;
			Gizmos.DrawRay(start, end);
		}
		
		start = Vector3.zero;
		end = new Vector3(screenResolution.x, 0, 0);
		for (float i = 0; i <= Mathf.RoundToInt(screenResolution.y); i+= unityGridSize)
		{
			start.y= i;
			Gizmos.DrawRay(start, end);
		}
		
	}
	
	public void SnapMaterialToGrid(DesignMaterial materialToSnap)
	{
		var materialTransform = materialToSnap.GetComponent<RectTransform>();
		Vector2 wantedPos = new Vector3 (Mathf.Ceil(materialTransform.anchoredPosition.x / unityGridSize) * unityGridSize, Mathf.Ceil(materialTransform.anchoredPosition.y / unityGridSize) * unityGridSize);
		materialTransform.anchoredPosition = wantedPos;
		
		Vector2 wantedSize = new Vector2(Mathf.Ceil(materialTransform.sizeDelta.x / unityGridSize) * unityGridSize, Mathf.Ceil(materialTransform.sizeDelta.y / unityGridSize) * unityGridSize);
		materialTransform.sizeDelta = wantedSize;
	}
	
	
	[Button(ButtonSizes.Medium), DisableIf("breakPointsOrdered")]
	void SortBreakpointsBySize()
	{
		myBreakpoints = myBreakpoints.OrderBy(x => x.dp).ToList<BreakpointsScriptableObject>();
		breakPointsOrdered = true;
	}
	
	[Button(ButtonSizes.Medium)]
	void SelectActualBreakpoint()
	{
		breakPointsOrdered = false;
		SortBreakpointsBySize();
		int i = 0;
		actualBreakPoint = myBreakpoints[0];
		foreach(BreakpointsScriptableObject breakpoint in myBreakpoints)
		{
			if(trueDp < breakpoint.dp)
			{
				if(i < myBreakpoints.Count)
				{
					
				}
				
				else if(i == myBreakpoints.Count)
				{
					actualBreakPoint = breakpoint;
				}
			}
			
			else
			{
				actualBreakPoint = breakpoint;
			}
			i++;
		}
		
		usedDp = actualBreakPoint.dp;
		ConvertDpToUnits();
		

	}
	
	void UpdateBreakpoints()
	{
		myBreakpoints.Clear();
		
		BreakpointsScriptableObject[] bpAtPath = MaterialUtilities.GetAtPath<BreakpointsScriptableObject>(BreakpointsPath);
		foreach(BreakpointsScriptableObject bso in bpAtPath)
		{
			myBreakpoints.Add(bso);
		}
	}
	
	void CalculateMaterialsSize()
	{
		int materialSize = (int)screenResolution.x - (actualBreakPoint.margin * 2 - actualBreakPoint.gutter * actualBreakPoint.columns) * actualBreakPoint.columns;
	}
	
	
	void OnValidate()
	{
		UpdateResolution();
		myMats.Clear();
		myMats = new List<DesignMaterial>();
		foreach(Transform child in gameObject.GetComponentInChildren<Transform>())
		{
			if(child.GetComponent<DesignMaterial>() != null)
			{
				myMats.Add(child.GetComponent<DesignMaterial>());
			}
			
		}
		
		myBreakpoints.Clear();
		UpdateBreakpoints();
	}
}
