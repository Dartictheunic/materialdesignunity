﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Material Design/Breakpoint", fileName = "New Breakpoint")]
public class BreakpointsScriptableObject : ScriptableObject
{
	public int dp;
	[SuffixLabel("dp", Overlay = true)]
	public int margin;
	[SuffixLabel("dp", Overlay = true)]
	public int gutter;
	[SuffixLabel("dp", Overlay = true)]
	public int columns;
}
