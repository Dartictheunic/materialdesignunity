﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class MaterialUtilities
{
	public static T[] GetAtPath<T> (string path) {
       
		ArrayList al = new ArrayList();
		string [] fileEntries = System.IO.Directory.GetFiles(path);
		foreach(string fileName in fileEntries)
		{
			int index = fileName.LastIndexOf("/");
			string localPath = fileName;
               
			Object t = AssetDatabase.LoadAssetAtPath(localPath, typeof(T));
			Debug.Log("Object " + fileName + " found at " + localPath);
			if(t != null)
			{
				al.Add(t);
				Debug.Log("Adding breakpoint " + t.name + " to list");
			}
			
			else
			{
				Debug.LogWarning("Null breakpoint, please make sure the good folder is selected and please only use it for breakpoints");
			}
		}
		T[] result = new T[al.Count];
		for(int i=0;i<al.Count;i++)
			result[i] = (T)al[i];
           
		return result;
	}
}
