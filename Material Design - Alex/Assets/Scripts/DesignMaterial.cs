﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class DesignMaterial : MonoBehaviour
{
	[TitleGroup("Debug", "You can ignore that as long as you don't have any error")]
	[ReadOnly, ShowInInspector]
	MaterialDesignCanvas canvas;
	[ReadOnly, ShowInInspector]
	RectTransform rectTransform;
	[ReadOnly, ShowInInspector]
	BoxCollider2D mycollider;
	
	void Awake()
	{
		ReassignMissingReferences();
	}
	
	[Title("Reassing missing references","In case it doesn't do automatically")]
	[Button(ButtonSizes.Large), GUIColor(.8f, .3f, .3f)]
	void ReassignMissingReferences()
	{
		UnityEditor.EditorGUIUtility.PingObject(gameObject);
		canvas = GetComponentInParent<MaterialDesignCanvas>();
		rectTransform = GetComponent<RectTransform>();
		mycollider = GetComponent<BoxCollider2D>();	
		canvas.SnapMaterialToGrid(this);
		
	}


	void OnDrawGizmos()
	{
		if(canvas != null)
		{
			canvas.SnapMaterialToGrid(this);
		}
		
		else
		{
			Debug.LogError("Missing references, if this problem persist please do it manually by going to Design Material > Settings > Reassign references");

			ReassignMissingReferences();
		}
		
		if(rectTransform != null && mycollider != null)
		{
			if (rectTransform.hasChanged)
			{
				Rect myRect = rectTransform.rect;
				mycollider.size = new Vector2(myRect.width, myRect.height);
				mycollider.offset = new Vector2(myRect.width/2, - myRect.height / 2);
			}
		}
		
		else
		{
			Debug.LogError("Missing references, if this problem persist please do it manually by going to Design Material > Settings > Reassign references");

			ReassignMissingReferences();
		}
		
		ContactFilter2D mycontacts = new ContactFilter2D();
		List<Collider2D> mylist = new List<Collider2D>();
		var mybox =  Physics2D.OverlapBox(mycollider.offset + rectTransform.rect.position, mycollider.size, 0);
		if(mycollider.OverlapCollider(mycontacts, mylist) > 0)
		{

			Debug.LogWarning("Intersection on material " + name + " with material " + mylist[0].name);
			
			foreach(Collider2D other in mylist)
			{
				DrawDebugRectangleForCollision(other);
			}
		}
		
	}
	
	void DrawDebugRectangleForCollision(Collider2D otherCol)
	{
		Vector3 myPointOfCollision = returnPointInOtherCollider(mycollider, otherCol);
		Vector3 otherPointOfCollision = returnPointInOtherCollider(otherCol, mycollider);

		Vector3 intersectionCenter = Vector3.Lerp(myPointOfCollision, otherPointOfCollision, .5f);
		
		Vector3 boxSize = returnCollisionSize(myPointOfCollision, otherPointOfCollision);
		Gizmos.color = Color.red;
		Gizmos.DrawCube(intersectionCenter, boxSize);

	}
	
	Vector3 returnCollisionSize(Vector3 myPoint, Vector3 otherPoint)
	{
		Vector3 collisionSize = Vector3.one;
		collisionSize.x = Mathf.Max(myPoint.x, otherPoint.x) - Mathf.Min(myPoint.x, otherPoint.x);
		collisionSize.y = Mathf.Min(myPoint.y, otherPoint.y) - Mathf.Max(myPoint.y, otherPoint.y);
		return collisionSize;
	}
	
	Vector3 returnPointInOtherCollider(Collider2D pointsToTest, Collider2D colliderToTest)
	{
		Vector3 pointInOtherCollider = Vector3.zero;
		List<Vector3> myColPoints = new List<Vector3>();
		Vector3 opposite = pointsToTest.transform.position + new Vector3(pointsToTest.bounds.size.x, -pointsToTest.bounds.size.y, 0);
		float maxX = opposite.x;
		float maxY = opposite.y;
		myColPoints.Add(transform.position);
		myColPoints.Add(new Vector3(maxX, transform.position.y, 0));
		myColPoints.Add(new Vector3(transform.position.x, maxY, 0));
		myColPoints.Add(opposite);
		
		foreach(Vector3 point in myColPoints)
		{
			if(colliderToTest.OverlapPoint(point))
			{
				pointInOtherCollider = point;
			}
		}
		
		Debug.Log(pointInOtherCollider);
		return pointInOtherCollider;
	}
	
	
	Vector3 ReturnCenteredPosition(Collider2D col)
	{
		Vector3 sizeScale = new Vector3(1/col.bounds.size.x, 1/col.bounds.size.y, 0);
		Vector3 goodPosition = Vector3.Scale(col.transform.position, sizeScale);
		
		return goodPosition;
	}
	
}